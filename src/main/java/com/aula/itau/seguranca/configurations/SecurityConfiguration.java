package com.aula.itau.seguranca.configurations;

import com.aula.itau.seguranca.auth.FiltroDeAutenticacao;
import com.aula.itau.seguranca.auth.FiltroDeAutorizacao;
import com.aula.itau.seguranca.auth.JWTUtil;
import com.aula.itau.seguranca.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity

public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UsuarioService usuarioService;

    private static final String[] PUBLIC_ENDPOINTS_POST = {
            "/usuarios",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.cors();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests().antMatchers(HttpMethod.POST, PUBLIC_ENDPOINTS_POST)
                .permitAll()
                .anyRequest()
                .authenticated();

        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroDeAutorizacao(authenticationManager(), jwtUtil, usuarioService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioService).passwordEncoder(constructorEncoder());
    }

    @Bean
    CorsConfigurationSource construtorDeCors(){
        UrlBasedCorsConfigurationSource cors =  new UrlBasedCorsConfigurationSource();

        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return cors;
    }

    @Bean
    BCryptPasswordEncoder constructorEncoder(){
        return new BCryptPasswordEncoder();
    }
}
